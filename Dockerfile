FROM alpine

MAINTAINER securefab (https://gitlab.com/securefab)

RUN apk update && \
apk add --no-cache openssl && \
rm -rf /var/cache/apk/*

ENTRYPOINT ["openssl"]
